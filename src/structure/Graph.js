/**
 * Created by Qiaomu on 2014/7/13.
 */
//SeedNodes={
//    nodes:[3,2,14,22,58,54,39,174]
//}
//da 22,58,54,39,3,2
//data 33,34
SeedNodes=[2357,156,1867,2909] //,
//SeedNodes=[];
Graph = function(){
    var Graph=function(data){
        this.graph=undefined;
        this.nodes=undefined;
        this.links=undefined;
        this.nodeHashMap={};
        this.edgeHashMap={};

        this.Top20=new Array(20);
        this.preferEdges=new Array();// Store the index not the id
        this.init(data);

//        this.actNodeList=new ActNodeList(50);

    }


    Object.defineProperties(Graph.prototype, {
        init:{
            value :function(data){
                this.graph=data;
                this.nodes=data.nodes;
                this.edges=data.links;
                for(var i=0;i<this.nodes.length;i++){
                    this.nodeHashMap["ID_"+this.nodes[i].id]=i;
                    var node=this.nodes[i];

                    node.getX=function(){
                        return this.graphics.x;
                    }
                    node.getY=function(){
                        return this.graphics.y;
                    }
                    node.getActivity=function(){
                        return this.activity;
                    }
                    node.enhance=function(value){
                        this.activity+=value;
                        this.totalActivity+=value;
                    }
                    node.cool=function(value){
                        this.activity-=value;
                    }

                    node.activity=0;
                    node.totalActivity=0;
                }

                for(var i=0;i<this.edges.length;i++){
                    for(var j=0;j<SeedNodes.length;j++){
                        if(this.edges[i].source==SeedNodes[j]||this.edges[i].target==SeedNodes[j]){
                            this.preferEdges.push(i);
                        }
                    }
                }
            }
        },
        getNodeById:{
            value:function(id){
                var index=this.nodeHashMap["ID_"+id];
                return this.nodes[index];
            }
        },
        getEdgeById:{
            value:function(id){
                var index=this.edgeHashMap["ID_"+id];
                return this.edges[index];
            }
        },
        randomEdges:{
            value:function(){
                var edgeId=Math.floor(Math.random()*this.edges.length);
            }
        },
        randomTriple:{
            value:function(){
                var edgeId=Math.floor(Math.random()*this.edges.length);
                return this.getTriple(edgeId);
            }
        },
        getNodes:{
            value:function(){
                return this.nodes;
            }
        },
        getEdges:{
            value:function(){
                return this.edges;
            }
        },
        preferRandomEdges:{
            value:function(value){
                var edgeId=Math.floor(Math.random()*(this.edges.length*value));
                var pLength=this.preferEdges.length;
                var eLength=this.edges.length;
                if(edgeId<eLength){
                    return this.getTriple(edgeId);
                }else{
                    var pId=Math.floor((edgeId-eLength)%pLength);
                    var eIndex=this.preferEdges[pId];

                    return this.getTriple(eIndex);
                }

            }
        },
        getTriple:{
            value:function(id){
                var sourceId=this.edges[id].source;
                var targetId=this.edges[id].target;
//                this.nodes[sourceId].enhance(1);
//                this.nodes[targetId].enhance(1);
//                this.actNodeList.setIn(this.nodes[sourceId]);
//                this.actNodeList.setIn(this.nodes[targetId]);
                return [id,sourceId,targetId];
            }
        },
        refreshPreList:{
            value:function(value){
                if(value!=undefined){
                    SeedNodes.push(value);
                }
                this.preferEdges=new Array();
                for(var i=0;i<this.edges.length;i++){
                    for(var j=0;j<SeedNodes.length;j++){
                        if(this.edges[i].source==SeedNodes[j]||this.edges[i].target==SeedNodes[j]){
                            this.preferEdges.push(i);
                        }
                    }
                }
            }
        }

    });
    return Graph;
}();

