/**
 * @author Lee Stemkoski   http://www.adelphi.edu/~stemkoski/
 */

///////////////////////////////////////////////////////////////////////////////

/////////////
// SHADERS //
/////////////

// attribute: data that may be different for each particle (such as size and color);
//      can only be used in vertex shader
// varying: used to communicate data from vertex shader to fragment shader
// uniform: data that is the same for each particle (such as texture)

particleVertexShader =
    [
        "attribute vec3  customColor;",
        "attribute float customOpacity;",
        "attribute float customSize;",
        "attribute float customAngle;",
        "varying vec4  vColor;",
        "varying float vAngle;",
        "void main()",
        "{",

        "vColor = vec4( customColor, customOpacity );", //     set color associated to vertex; use later in fragment shader.

        "vAngle = customAngle;",

        "vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );",
        "gl_PointSize = customSize * ( 300.0 / length( mvPosition.xyz ) );",     // scale particles as objects in 3D space
        "gl_Position = projectionMatrix * mvPosition;",
        "}"
    ].join("\n");

particleFragmentShader =
    [
        "uniform sampler2D texture;",
        "varying vec4 vColor;",
        "varying float vAngle;",
        "void main()",
        "{",
        "gl_FragColor = vColor;",

        "float c = cos(vAngle);",
        "float s = sin(vAngle);",
        "vec2 rotatedUV = vec2(c * (gl_PointCoord.x - 0.5) + s * (gl_PointCoord.y - 0.5) + 0.5,",
        "c * (gl_PointCoord.y - 0.5) - s * (gl_PointCoord.x - 0.5) + 0.5);",  // rotate UV coordinates to rotate texture
        "vec4 rotatedTexture = texture2D( texture,  rotatedUV );",
        "gl_FragColor = gl_FragColor * rotatedTexture;",    // sets an otherwise white particle texture to desired color

        "}"
    ].join("\n");

///////////////////////////////////////////////////////////////////////////////

/////////////////
// TWEEN CLASS //
/////////////////



///////////////////////////////////////////////////////////////////////////////

////////////////////
// PARTICLE CLASS //
////////////////////

function Particle( node )
{
    this.position     = new THREE.Vector3();
    this.size = 12;
    this.alive = 1; // use float instead of boolean for shader purposes

    //var z = 500 - node.ModularityClass*50 ;
    var z = 0;
    node.graphics.z =z;
    var x = node.graphics.x || ((Math.random() -0.5)*500);
    var y = node.graphics.y || ((Math.random() -0.5)*500);
    this.position.set(x,y,z);

    this.color = new THREE.Color(node.graphics.fill);
    this.opacity = 1;
    this.commonSize = 12.0;

    this.activeAge = 0;
}

function Edge(snode, tnode){
    var sx = snode.graphics.x;
    var sy = snode.graphics.y;
    var tx = tnode.graphics.x;
    var ty = tnode.graphics.y;
    this.sourceNode = snode;
    this.targetNode = tnode;


    this.color = new THREE.Color(0xFFFFFF);
    this.opacity = 1;
   

    this.activeAge = 0;
}

///////////////////////////////////////////////////////////////////////////////

///////////////////////////
// PARTICLE ENGINE CLASS //
///////////////////////////

var Type = Object.freeze({ "CUBE":1, "SPHERE":2 });

function ParticleEngine( graph )
{
    this.formatGraph = new Graph(graph);
    this.g = graph;

    this.particleArray = [];
    this.edgeArray = [];
    //Record;
    this.geoEdge = [];

    this.particleGeometry = new THREE.Geometry();
    this.particleTexture  = THREE.ImageUtils.loadTexture( 'textures/disc.png' );

    this.particleMesh = new THREE.Mesh();

    this.record  = []
    for(var i=0;i< this.g.links.length;i++){
        this.record[i] = 0;
    }

    this.particleGeometry = new THREE.Geometry();
    this.particleMaterial = new THREE.ShaderMaterial(
        {
            uniforms:
            {
                texture:   { type: "t", value: this.particleTexture }
            },
            attributes:
            {
                customVisible:	{ type: 'f',  value: [] },
                customSize:		{ type: 'f',  value: [] },
                customColor:	{ type: 'c',  value: [] },
                customOpacity:	{ type: 'f',  value: [] }
            },
            vertexShader:   particleVertexShader,
            fragmentShader: particleFragmentShader,
            transparent: true,  alphaTest: 0.5, // if having transparency issues, try including: alphaTest: 0.5,
            blending: THREE.NormalBlending, depthTest: true
        });
}

ParticleEngine.prototype.createParticle = function( node )
{
    var particle = new Particle(node);
    return particle;
}

ParticleEngine.prototype.createEdge = function( ed ){
    var snode = this.g.nodes[ed.source];
    var tnode = this.g.nodes[ed.target];

    var edge = new Edge(snode,tnode);
    return edge;
}

ParticleEngine.prototype.initialize = function( )
{
    // link particle data with geometry/material data

    for (var i = 0; i < this.g.nodes.length; i++)
    {
        // remove duplicate code somehow, here and in update function below.
        this.particleArray[i] = this.createParticle(this.g.nodes[i]);

        this.particleGeometry.vertices[i] = this.particleArray[i].position;
        this.particleMaterial.attributes.customVisible.value[i] = this.particleArray[i].alive;
        this.particleMaterial.attributes.customColor.value[i]   = this.particleArray[i].color;//this.g.nodes[i].graphics.fill;
        this.particleMaterial.attributes.customOpacity.value[i] = this.particleArray[i].opacity;
        this.particleMaterial.attributes.customSize.value[i]    = this.particleArray[i].size;
        //this.particleMaterial.attributes.customAngle.value[i]   = this.particleArray[i].angle;
    }
    for( var i = 0; i< this.g.links.length ; i++){
        this.edgeArray[i] = this.createEdge(this.g.links[i]);

        var lineGeometry = new THREE.Geometry();

        lineGeometry.vertices.push(new THREE.Vector3(
                this.edgeArray[i].sourceNode.graphics.x,
                this.edgeArray[i].sourceNode.graphics.y,
                this.edgeArray[i].sourceNode.graphics.z
            ),
            new THREE.Vector3(
                this.edgeArray[i].targetNode.graphics.x,
                this.edgeArray[i].targetNode.graphics.y,
                this.edgeArray[i].targetNode.graphics.z
            )
        )
        var lineMaterial = new THREE.LineBasicMaterial( {
            color: 0xffffff,
            opacity:1,
            linewidth:0.5,
            transparent: true,
            visible :true

        } );
        // var lineMaterial = new THREE.LineBasicMaterial();

        var line = new THREE.Line( lineGeometry, lineMaterial );

        //line.material.opacity = 0.1;
        this.geoEdge[i] = line;
        //scene.add(line);


    }

//    this.particleMaterial = new THREE.MeshBasicMaterial({
//        color: 'red'
//    });
    this.particleMesh = new THREE.PointCloud( this.particleGeometry, this.particleMaterial );
    this.particleMesh.dynamic = true;
    this.particleMesh.sortParticles = true;
    scene.add( this.particleMesh );

}
var times=0;
var t=0;
var vt=1;

ParticleEngine.prototype.update = function(dt)
{
    if(times<50) t=1;
    else if(times<90) t=Math.round(times/20)
    else {
        t=6;
        vt=3
    }

    for(var i=0;i<t;i++){
        var est= this.formatGraph.preferRandomEdges(vt);//,156,1867,2909
        var edgeIndex = est[0];
        if(times==60){

            g.refreshPreList(156);

        }
        if(times==90) g.refreshPreList(1867);
        if(times==120) {
            g.refreshPreList(2909);
        }

        this.updateEdges(edgeIndex);
        var sid = this.g.links[edgeIndex].source;
        var tid = this.g.links[edgeIndex].target;
        this.updateNodes(sid);
        this.updateNodes(tid);

    }
    times++;

}
ParticleEngine.prototype.updateNodes = function(num){
    var recycleIndices = [];

    // update particle data
    //var num = Math.ceil( Math.random() * (this.g.nodes.length-1));

    this.particleArray[num].activeAge = 3;

    for (var i = 0; i < this.g.nodes.length; i++)
    {
        if(this.particleArray[i].activeAge>0){

            this.particleMaterial.attributes.customColor.value[i]   = this.particleArray[i].color;

            this.particleMaterial.attributes.customSize.value[i]    = this.particleArray[i].commonSize * (this.particleArray[i].activeAge + 1);
            this.particleMaterial.attributes.customOpacity.value[i] = this.particleArray[i].opacity * (this.particleArray[i].activeAge + 1);
            this.particleArray[i].activeAge -= 0.01;
        }
    }
}
ParticleEngine.prototype.updateEdges = function(num){

    //var num = Math.ceil(Math.random() * this.g.links.length);
    this.edgeArray[num].activeAge = 3

    for(var i=0;i< this.geoEdge.length;i++){
        if(this.edgeArray[i].activeAge>0.1){
            if(this.record[i]!=0)
            {
                this.edgeArray[i].activeAge -=0.01;
                this.edgeArray[i].opacity -= 0.01;

                this.geoEdge[i].material.setValues({opacity : this.edgeArray[i].opacity});
            }else{
                scene.add(this.geoEdge[i]);
            }


        }else if(this.edgeArray[i].activeAge>0){
            this.edgeArray[i].opacity = 1;
            this.record[i] = 0;
            scene.remove(this.geoEdge[i]);

        }
    }
    this.record[num] = 1;

}
ParticleEngine.prototype.destroy = function()
{
    scene.remove( this.particleMesh );
}
///////////////////////////////////////////////////////////////////////////////
