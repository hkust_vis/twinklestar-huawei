/**
 * Created by root on 14-8-9.
 */

var MainEnv = MainEnv || {};

MainEnv.Scenario = function(params) {
    this.container = document.getElementById(params.container);
    this.width = params.width;
    this.height = params.height;

    this.camera = null;
    this.stats = null;
    this.graph = null;
    this.scene = null;
    this.renderer = null;
    this.particles = [];
    this.controls = null;

    this.projector = null;
    this.raycaster = null;
    this.intersects = null;
    this.links = [];
    this.selectLinks = [];
    this.selectNodes = [];
    this.isEdgeVisiable = false;

    this.vector = null;
    this.mouse = {x: 0, y: 0};
};

MainEnv.Scenario.prototype.force = d3.layout.force()
    .charge(-120)
    .linkDistance(60)
    .gravity(0.5)
    .size([this.width, this.height]);

MainEnv.Scenario.prototype.initSelector = function(){//Init the interaction functions
    this.projector = new THREE.Projector();
    this.raycaster = new THREE.Raycaster();
};


MainEnv.Scenario.prototype.init = function () {
    this.camera = new THREE.PerspectiveCamera(45, this.width / this.height, 1, 100000);
    this.camera.position.set(0, 0, 2000);
    this.scene = new THREE.Scene();
    this.initSelector();
    var material = new THREE.SpriteCanvasMaterial({
        color: 0xffffff,
        program: function(context) {
            context.beginPath();
            context.arc(0, 0, 0.5, 0, Math.PI * 2, true);
            context.fill();
        }

    });

    for (var i = 0; i < this.graph.nodes.length; i++) {
        var particle = new THREE.Sprite(material);
        particle.position.set(this.graph.nodes[i].x, this.graph.nodes[i].y, 0);
        particle.scale.set(5, 5, 5);
        particle.index = i;
        particle.prttp = this.graph.nodes[i]; //Record of graph nodes
        this.particles.push(particle);

        this.scene.add(particle);
    }
    for (var i = 0; i < this.graph.links.length; i++) {
        var lineGeometry = new THREE.Geometry();
        var source = this.graph.links[i].source;
        var target = this.graph.links[i].target;
        if (typeof(source) == 'object' && typeof(target) == 'object') {
            source = source.index;
            target = target.index;
        }
        lineGeometry.vertices.push(this.particles[source].position);
        lineGeometry.vertices.push(this.particles[target].position);
        var lineMaterial = new THREE.LineBasicMaterial({
            color: 'rgb(134,131,108)',
            opacity: 0.3,
            linewidth: 0.2,
            transparent: true,
            visible: true
        });
        var line = new THREE.Line(lineGeometry, lineMaterial);
        this.links.push(line);
        //this.scene.add(line);
    }

    material.color = new THREE.Color('rgb(31,119,108)');
    //material.color = new THREE.Color('rgb(134,131,108)');
    this.renderer = new THREE.CanvasRenderer();
    this.renderer.setClearColor(0xffffff0);

    this.renderer.setSize(this.width, this.height);
    this.container.appendChild(this.renderer.domElement);

    this.stats = new Stats();
    this.stats.domElement.style.position = 'absolute';
    this.stats.domElement.style.top = '0px';
    this.container.appendChild(this.stats.domElement);
    //
    this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

};

MainEnv.Scenario.prototype.updateParticles = function() {
    for (var i = 0; i < this.graph.nodes.length; i++) {
        this.particles[i].position.set(
                this.graph.nodes[i].x - this.width / 2,
                this.graph.nodes[i].y - this.height / 2,
                0);
    }
};

MainEnv.Scenario.prototype.edgeVisiable = function() {
    if (this.isEdgeVisiable) {
        this.isEdgeVisiable = false;
        for (var i = 0; i < this.links.length; i++) {
            this.scene.remove(this.links[i]);
        }
    } else {
        this.isEdgeVisiable = true;
        for (var i = 0; i < this.links.length; i++) {
            this.scene.add(this.links[i]);
        }
    }
};

MainEnv.Scenario.prototype.initDGraph = function(data) {
    this.graph = new DGraph(data);
};

MainEnv.Scenario.prototype.modifyMouse = function(x, y) {
    this.mouse.x = ((x - this.renderer.domElement.offsetLeft) /
            this.renderer.domElement.clientWidth) * 2 - 1;
    this.mouse.y = -((y - this.renderer.domElement.offsetTop) /
            this.renderer.domElement.clientHeight) * 2 + 1;
};

MainEnv.Scenario.prototype.update = function() {
    this.vector = new THREE.Vector3(this.mouse.x, this.mouse.y, 1);
    this.projector.unprojectVector(this.vector, this.camera);
    this.raycaster.ray.set(this.camera.position,
            this.vector.sub(this.camera.position).normalize());
    this.intersects = this.raycaster.intersectObjects(this.particles);

    if (this.intersects.length != 0) {
        var selectIndex = this.intersects[0].object.index;
        var particle = this.particles[selectIndex];
        particle.scale.set(particle.prttp.customeSize * 2.5,
                particle.prttp.customeSize * 2.5,
                particle.prttp.customeSize * 2.5);
        new TWEEN.Tween(particle.scale)
            .delay(100)
            .to({
                x: particle.prttp.customeSize,
                y: particle.prttp.customeSize,
                z: particle.prttp.customeSize
            }, 1000)
            .start();
        for (var i = 0; i < particle.prttp.sourceLinks.length; i++) {
            var sourceIndex = particle.prttp.sourceLinks[i].index;

            var lineGeometry = new THREE.Geometry();
            var source = this.graph.links[sourceIndex].source;
            var target = this.graph.links[sourceIndex].target;
            if (typeof(source) == 'object' && typeof(target) == 'object') {
                source = source.index;
                target = target.index;
            }
            lineGeometry.vertices.push(this.particles[source].position);
            lineGeometry.vertices.push(this.particles[target].position);
            var lineMaterial = new THREE.LineBasicMaterial({
                color: 'rgb(134,131,108)',
                opacity: 0.8,
                linewidth: 0.8,
                transparent: true,
                visible: true
            });
            var line = new THREE.Line(lineGeometry, lineMaterial);
            this.selectLinks.push(line);
            this.scene.add(line);

        }
        for (var i = 0; i < particle.prttp.targetLinks.length; i++) {
            var targetIndex = particle.prttp.targetLinks[i].index;
            var lineGeometry = new THREE.Geometry();
            var source = this.graph.links[targetIndex].source;
            var target = this.graph.links[targetIndex].target;
            if (typeof(source) == 'object' && typeof(target) == 'object') {
                source = source.index;
                target = target.index;
            }
            lineGeometry.vertices.push(this.particles[source].position);
            lineGeometry.vertices.push(this.particles[target].position);
            var lineMaterial = new THREE.LineBasicMaterial({
                color: 'rgb(134,131,108)',
                opacity: 0.8,
                linewidth: 0.8,
                transparent: true,
                visible: true
            });
            var line = new THREE.Line(lineGeometry, lineMaterial);
            this.selectLinks.push(line);
            this.scene.add(line);
        }
    } else {
            for (var i = 0; i < this.selectLinks.length; i++) {
                this.scene.remove(this.selectLinks[i]);
            }
            this.selectLinks = [];
    }

    this.controls .update();
    this.render();
    this.stats.update();
    this.updateParticles();
};

MainEnv.Scenario.prototype.render = function (){
    this.renderer.render(this.scene, this.camera);
    TWEEN.update();
};
