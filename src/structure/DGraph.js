/**
 * Created by Qiaomu on 14-8-9.
 */
DGraph = function(){
    var DGraph=function(data){
        this.nodes=data.nodes;
        this.links=data.links;
        this.init();
    }


    Object.defineProperties(DGraph.prototype, {
        init: {
            value: function () {
                for(var i=0; i<this.nodes.length;i++){
                    this.nodes[i].sourceLinks = [];
                    this.nodes[i].targetLinks = [];
                    this.nodes[i].customeSize = 5;
                }
                for(var i=0; i<this.links.length;i++){
                    var source = this.links[i].source;
                    var target = this.links[i].target;
                    this.links[i].index = i;
                    if(typeof(source) == 'object'&&typeof(target)=='object'){
                        source = source.index;
                        target = target.index;
                    }
                    this.nodes[source].sourceLinks.push(this.links[i]);
                    this.nodes[target].targetLinks.push(this.links[i]);
                }
            }
        },
        lsSort:{
            value :function () {
                this.sortArray.sort(function(a,b){
                    return b-a;
                })
            }
        },
        test:{
            value : function(){
                this.sortNode = this.sortArray.slice(0,this.sortArray.length);
                this.lsSort();
                var x;
            }
        }


    });
    return DGraph;
}();
