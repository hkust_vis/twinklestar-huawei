/**
 * Created by Qiaomu on 2014/7/10.
 */


MapTo = {};
Object.defineProperties(MapTo,{
    map_Points_To_Region: {
        value: function (points, nix_start, nix_end, niy_start, niy_end,gpobj) {
            var max_x = gpobj.getX(points[0]);
            var min_x = gpobj.getX(points[0]);
            var max_y = gpobj.getY(points[0]);
            var min_y = gpobj.getY(points[0]);

            for (var i = 1; i < points.length; i++) {
                if (gpobj.getX(points[i]) == undefined || gpobj.getY(points[i]) == undefined) continue;
                if (max_x < gpobj.getX(points[i])) {
                    max_x =gpobj.getX(points[i]);
                }
                if (min_x > gpobj.getX(points[i])) {
                    min_x = gpobj.getX(points[i])

                }
                if (max_y < gpobj.getY(points[i])){
                    max_y =  gpobj.getY(points[i]);
                }
                if (min_y >  gpobj.getY(points[i])) {
                    min_y =  gpobj.getY(points[i]);
                }
            }


            for (var i = 0; i < points.length; i++) {
                //absolute_position[i] = new Array();
                gpobj.setX(points[i],this.mapTo(gpobj.getX(points[i]), min_x, max_x, nix_start, nix_end)) ;
                gpobj.setY(points[i],this.mapTo(gpobj.getY(points[i]), min_y, max_y, niy_start, niy_end));
            }

        }
    },
    mapTo:{
        value: function(x,oi_start,oi_end,ni_start,ni_end){
            //mapTo a value from an old interval to a new interval
            if(x==undefined) return undefined;
            else  return (x-oi_start)/(oi_end-oi_start)*(ni_end-ni_start)+ni_start;
        }
    }
})


gpobj={};
Object.defineProperties(gpobj,{
    getX:{
        value:function(point){
            return point.graphics.x;
        }
    },
    getY:{
        value:function(point){
            return point.graphics.y;
        }
    },
    setX:{
        value:function(point,value){
            point.graphics.x=value;
        }
    },
    setY:{
        value:function(point,value){
            point.graphics.y=value;
        }
    }
})
