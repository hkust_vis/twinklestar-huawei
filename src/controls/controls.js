/**
 * Created by Qiaomu on 2014/7/24.
 */
function onKeyDown ( event ) {

    switch( event.keyCode ) {


        case 83: /*W*/
            camera.position.z+=10;
            break;

        case 87: /*P*/
            camera.position.z-=10;
            break;

        case 65:
            camera.position.x-=10;
            break;
        case 68:
            camera.position.x+=10;
            break;
        case 81:
            camera.position.y+=10;
            break;
        case 90:
            camera.position.y -=10;
            break;
        case 37:
            camera.rotation.y += 0.1;
            break;
        case 39:
            camera.rotation.y -= 0.1;
            break;
        case 77:

            camera.position.x = 1500;
            camera.position.y = 0;
            camera.position.z = 0;
            camera.lookAt({
                x : 0,
                y : 0,
                z : 0
            });

            camera.rotation.y -= 5.4;
            camera.rotation.x += Math.PI;
            break;
        case 78:
            camera.position.x = 0;
            camera.position.y = 0;
            camera.position.z = 1500;

//            camera.up.x = 0;
//            camera.up.y = 0;
//            camera.up.z = 0;
            camera.lookAt({
                x : 0,
                y : 0,
                z : 0
            });
            break;

    }

};